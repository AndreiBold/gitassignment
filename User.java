package ro.ase.csie.cts.g1085.temagit;

public class User implements CaloricTracker{

	public String name;
	public double weightInKgs;
	public double bodyFatPercentage;
	public ActivityLevel activityLevel;
	
	public final static double MULTIPLY_VALUE = 21.6;
	public final static int ADD_VALUE = 370;
	public final static double MULTIPLY_VALUE_SEDENTARY = 1.2;
	public final static double MULTIPLY_VALUE_MODERATE = 1.45;
	public final static double MULTIPLY_VALUE_ACTIVE = 1.67;
	public final static double MULTIPLY_VALUE_VERY_ACTIVE = 1.9;

	

	public User(String name, double weightInKgs, double bodyFatPercentage, ActivityLevel activityLevel) {
		super();
		this.name = name;
		this.weightInKgs = weightInKgs;
		this.bodyFatPercentage = bodyFatPercentage;
		this.activityLevel = activityLevel;
	}

	public String getName() {
		return name;
	}

	public double getDailyBMR() throws Exception {
		return MULTIPLY_VALUE * this.getFatFreeMass() + ADD_VALUE;
		
	}
	
	public double getFatFreeMass() throws Exception {
		return this.weightInKgs - this.getMassWithFat();
	}
	
	public double getMassWithFat() throws Exception {
		if(this.weightInKgs < 0 || this.bodyFatPercentage < 0)
			throw new Exception("Please enter valid information!");
		else {
			return this.bodyFatPercentage * this.weightInKgs;
		}
	}

	@Override
	public double getCaloriesForMentenance(User user) throws Exception {
		switch(user.activityLevel) {
		case SEDENTARY:
			return user.getDailyBMR() * MULTIPLY_VALUE_SEDENTARY;
		case MODERATE:
			return user.getDailyBMR() * MULTIPLY_VALUE_MODERATE;
		case ACTIVE:
			return user.getDailyBMR() * MULTIPLY_VALUE_ACTIVE;
		case VERY_ACTIVE:
			return user.getDailyBMR() * MULTIPLY_VALUE_VERY_ACTIVE;
		default:
			throw new Exception("Choose a valid level of activity");
		}
	}

}
