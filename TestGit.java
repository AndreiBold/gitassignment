package ro.ase.csie.cts.g1085.temagit;

public class TestGit {

	public static void main(String[] args) throws Exception {
		System.out.println("Hello Git!\n" + 
	               "Denumirea proiectului de licenta este: Asistent conversational in fitness");
				
		User user = new User("Gigel", 80.30, 0.109, ActivityLevel.MODERATE);
		
		System.out.println("\nUser " + user.getName() +
				" has a weight mentenance level of " + 
				Math.round(user.getCaloriesForMentenance(user)) + " calories per day");
	}
}
