package ro.ase.csie.cts.g1085.temagit;

public enum ActivityLevel {
	
	SEDENTARY,
	MODERATE,
	ACTIVE,
	VERY_ACTIVE
}
