package ro.ase.csie.cts.g1085.temagit;

public interface CaloricTracker {

	public abstract double getCaloriesForMentenance(User user) throws Exception;
}
